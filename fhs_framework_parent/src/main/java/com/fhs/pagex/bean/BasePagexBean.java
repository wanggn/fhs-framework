package com.fhs.pagex.bean;

import com.fhs.common.constant.Constant;
import com.fhs.core.base.bean.BaseDO;
import com.fhs.core.trans.Trans;

/**
 * 描述
 *
 * @ProjectName: framework_v2_idea2
 * @Package: com.fhs.pagex.bean
 * @ClassName: BasePagexBean
 * @Author: JackWang
 * @CreateDate: 2018/12/29 0029 12:33
 * @UpdateUser: JackWang
 * @UpdateDate: 2018/12/29 0029 12:33
 * @Version: 1.0
 */
public class BasePagexBean<T extends BasePagexBean>  extends BaseDO<T> {

}
