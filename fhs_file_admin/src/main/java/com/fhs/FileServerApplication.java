package com.fhs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 文件服务
 * @author wanglei
 */
@SpringBootApplication
@MapperScan(basePackages = {"com.fhs.**.dao"})
@EnableConfigurationProperties
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.fhs"})
@EnableCaching
public class FileServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileServerApplication.class, args);
	}
}
